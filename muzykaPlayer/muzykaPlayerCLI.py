# This program implements a small muzyka client
# for demonstration purposes.
# It uses the MuzykaClient library which implements the muzyka Protocol.
# 
# Call this program from the commandline for more usage details.

import socket
import muzykaProtocol
from muzykaProtocol.muzykaFrame import muzykaFrame
from MuzykaClient import MuzykaClient
import QueueEntry
from QueueEntry import QueueEntry
import sys
from PlayerControl import PlayerControl
import Helper
def printUsage():
	print "USAGE: muzykaPlayerCLI.py COMMAND param"
	print ""
	print "Possible commands are:"
	print "LIST"
	print "ADD_SONG <song_id>"
	print "ADD_FILE <filepath>"
	print "ADD_URL http://<path>"
	print "REMOVE_ENTRY <queueId>"
	print "SET_CURRENT_QUEUE_ENTRY <queueId>"
	print "PLAY"
	print "STOP"
	print "PAUSE"
	print "NEXT"
	print "PREV"
	print "STATUS"
	
if len(sys.argv)==1:
	printUsage()
	sys.exit(1)

mc = MuzykaClient("/tmp/muzyka.sock")

if sys.argv[1]=="LIST":
	queueList =  mc.listQueue()
	i=0

	print "pos\ttype\treference"
	print "---------------------------"
	for e in queueList:
		print str(i)+"# \t" + str(e.entryType) + "\t" + str(e.reference)
		# print e.entryType,
		# print e.reference
		i+=1
elif sys.argv[1]=="ADD_SONG":
	if len(sys.argv) < 3:
		print "\nERROR: ADD_SONG requires a parameter\n"
		printUsage()
		sys.exit(1)
	else:
		mc.addQueueItem( QueueEntry(QueueEntry.TYPE_SONG, int(sys.argv[2])) )
		print "Item added to the players queue"
elif sys.argv[1]=="ADD_URL":
	if len(sys.argv) < 3:
		print "\nERROR: ADD_URL requires a parameter\n"
		printUsage()
		sys.exit(1)
	else:
		ret = mc.addQueueItem( QueueEntry(QueueEntry.TYPE_URL, sys.argv[2]) )
		if ret:
			print "Item added to the players queue"
		else:
			print "There has been an error when adding your file to the players queue. Maybe its not reachable by the server"

elif sys.argv[1]=="ADD_FILE":
	if len(sys.argv) < 3:
		print "\nERROR: ADD_FILE requires a parameter\n"
		printUsage()
		sys.exit(1)
	else:
		ret = mc.addQueueItem( QueueEntry(QueueEntry.TYPE_FILE, sys.argv[2]) )
		if ret:
			print "Item added to the players queue"
		else:
			print "There has been an error when adding your file to the players queue. Maybe its not reachable by the server"
		
elif sys.argv[1]=="REMOVE_ENTRY":
	if len(sys.argv) < 3:
		print "\nERROR: REMOVE_ENTRY requires a parameter\n"
		printUsage()
		sys.exit(1)
	else:
		mc.removeQueueEntry(sys.argv[2])
		
elif sys.argv[1]=="SET_CURRENT_QUEUE_ENTRY":
	if len(sys.argv) < 3:
		print "\nERROR: SET_CURRENT_QUEUE_ENTRY requires a parameter\n"
		printUsage()
		sys.exit(1)
	else:
		mc.setCurrentQueueEntry(sys.argv[2])

elif sys.argv[1]=="PLAY":
	mc.playerPlay()
elif sys.argv[1]=="STOP":
	mc.playerStop()
elif sys.argv[1]=="PAUSE":
	mc.playerPause()
elif sys.argv[1]=="NEXT":
	mc.playerNext()
elif sys.argv[1]=="PREV":
	mc.playerPrev()
elif sys.argv[1]=="STATUS":
	status = mc.playerStatus()
	print "Player status:"
	print "----------------"
	print "The current queue entry is " + str(status["cqe"])
	print "State:",
	print PlayerControl.stateToString(status["state"])
	if status["state"] != PlayerControl.STATE_STOP:
		print "Time: " + Helper.returnHourMinuteSecondString(status["secPlayed"]) + "/" + Helper.returnHourMinuteSecondString(status["duration"])
else:
	print "Unknown command"
		
mc.closeSocket()