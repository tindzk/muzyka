# This class models every single entry of the
# players queue. QueueEntry objects can have multiple types.
# It could be a song in the database, a file on the muzykaPlayers local disk,
# a playlist in the database or a network stream.
# 
# Every QueueEntry holds a "reference" to the actual audio data.
# The reference depends on the QueueEntrys type.
# See the comments for QueueEntry.reference for details.

class QueueEntry:
	entryType = None
	
	TYPE_SONG	= 1
	TYPE_FILE = 2
	TYPE_PLAYLIST = 3
	TYPE_URL = 4
	
	# Do we really need prefixes if we track the queue entrys type?
	# @discuss
	# 
	# The reference holds the "adress" of the
	# queue entry
	# It could be:
	# http://.... if entryType is TYPE_STREAM
	# file://.... if entryType is TYPE_FILE
	# song://.... if entryType is TYPE_PLAYLIST
	# playlist://.... if entryType is TYPE_SONG
	reference = None
	
	# Every queue entry must have a string representation
	def __init__(self,entryType,reference):
		"""Constructor"""
		self.entryType = entryType
		self.reference = reference
	
	def toString(self):
		"""String representation of a queue entry"""
		return "TYPE: " + str(self.entryType) + " " + str(self.reference)
		
		
