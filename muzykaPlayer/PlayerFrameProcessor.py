# This class defines methods to process a PLAYER-related frame
# It will check a given frame, trigger the necessary actions and then return a
# response in form of a muzykaFrame

from FrameProcessor import FrameProcessor
from muzykaProtocol.muzykaFrame import muzykaFrame
import muzykaProtocol

# TODO: PLAYER_STATUS

class PlayerFrameProcessor(FrameProcessor):	
	def __init__(self, playerControl):
		"""docstring for __init__"""
		self.playerControl = playerControl
		
	# This methods processes a given frame and takes the necessary actions
	# @param receivedFrame a muzykaFrame Object
	# @return a muzykaFrame Object with the response
	# @staticmethod
	def process(self,receivedFrame):
		"""Frame handling for PLAYER-related frames"""
		responseFrame = muzykaFrame()
		
		responseFrame.commandgroup	=	receivedFrame.commandgroup
		responseFrame.subcommand		=	receivedFrame.subcommand
		
		if receivedFrame.subcommand == muzykaProtocol.PLAYER_PLAY:
			self.playerControl.play()
			responseFrame.payload 			= muzykaProtocol.SERVER_ACK
			return responseFrame
		elif receivedFrame.subcommand == muzykaProtocol.PLAYER_STOP:
			self.playerControl.stop()
			responseFrame.payload 			= muzykaProtocol.SERVER_ACK
			return responseFrame
		elif receivedFrame.subcommand == muzykaProtocol.PLAYER_PAUSE:
			self.playerControl.pause()
			responseFrame.payload 			= muzykaProtocol.SERVER_ACK
			return responseFrame
		elif receivedFrame.subcommand == muzykaProtocol.PLAYER_NEXT:
			self.playerControl.next()
			responseFrame.payload 			= muzykaProtocol.SERVER_ACK
			return responseFrame
		elif receivedFrame.subcommand == muzykaProtocol.PLAYER_PREV:
			self.playerControl.prev()
			responseFrame.payload 			= muzykaProtocol.SERVER_ACK
			return responseFrame
		elif receivedFrame.subcommand == muzykaProtocol.PLAYER_STATUS:
			responseFrame.payload 			= self.playerControl.playerStateToYAML()
			return responseFrame
		else:
			print "processPlayerFrame doesnt know how to handle subcommand " + ("%#x" % ord(receivedFrame.subcommand))
			raise NotImplementedError
		
