# This class is a client library for the muzykaPlayer
# Server. It implements the interface between a 
# concrete muzykaClient (Webservice, CLI, DisplayClient)
# and the muzykaPlayer as the server.

# TODO: Test TYPE_URL Handling
# TODO: Minimize code duplication

import socket
import muzykaProtocol
from muzykaProtocol.muzykaFrame import muzykaFrame
import QueueEntry
from QueueEntry import QueueEntry
import yaml

class MuzykaClient:
	FRAME_SIZE = 16384
	
	def __init__(self, sockpath):
		self.initSocket(sockpath)
		
	# Sends a muzyka Frame and returns the response as muzykaFrame Object
	def sendMuzykaFrame(self, mf):
		self.sock.send( mf.toString() )

		# Read response
		data = self.sock.recv(self.FRAME_SIZE)
		response = muzykaFrame()
		response.parseFrame(data)
		
		return response
		
	def initSocket(self, sockpath):
		self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
		self.sock.connect(sockpath)
		
	def closeSocket(self):
		self.sock.close()
		
	# Play the queue entry which is referenced by PlayerContro.currentQueueEntry
	# 
	# @return true if a valid response has been received after
	# executing the PLAY command
	# @return false otherwise
	def playerPlay(self):
		# Create a muzyka frame with the request
		mf = muzykaFrame( muzykaProtocol.PLAYER, muzykaProtocol.PLAYER_PLAY, "")
		response = self.sendMuzykaFrame(mf)
		
		if response.payload == muzykaProtocol.SERVER_ACK:
			return True
		else:
			return False
	
	# Tell the muzykaPlayer to stop the playback
	# 
	# @return true if a valid response has been received after
	# executing the STOP command
	# @return false otherwise
	def playerStop(self):
		# Create a muzyka frame with the request
		mf = muzykaFrame( muzykaProtocol.PLAYER, muzykaProtocol.PLAYER_STOP, "")
		response = self.sendMuzykaFrame(mf)
		
		if response.payload == muzykaProtocol.SERVER_ACK:
			return True
		else:
			return False
	
	# Tell the muzykaPlayer to pause the current playback.
	# 
	# @return true if a valid response has been received after
	# executing the PAUSE command
	# @return false otherwise
	def playerPause(self):
		# Create a muzyka frame with the request
		mf = muzykaFrame( muzykaProtocol.PLAYER, muzykaProtocol.PLAYER_PAUSE, "")
		response = self.sendMuzykaFrame(mf)
		
		if response.payload == muzykaProtocol.SERVER_ACK:
			return True
		else:
			return False
		
	# Play the next entry in the queue.
	# 
	# @return true if a valid response has been received after
	# executing the NEXT command
	# @return false otherwise
	def playerNext(self):
		# Create a muzyka frame with the request
		mf = muzykaFrame( muzykaProtocol.PLAYER, muzykaProtocol.PLAYER_NEXT, "")
		response = self.sendMuzykaFrame(mf)
		
		if response.payload == muzykaProtocol.SERVER_ACK:
			return True
		else:
			return False

	# Play the previous entry in the queue.
	# 
	# @return true if a valid response has been received after
	# executing the PREV command
	# @return false otherwise
	def playerPrev(self):
		# Create a muzyka frame with the request
		mf = muzykaFrame( muzykaProtocol.PLAYER, muzykaProtocol.PLAYER_PREV, "")
		response = self.sendMuzykaFrame(mf)
		
		if response.payload == muzykaProtocol.SERVER_ACK:
			return True
		else:
			return False


	# Sends the QUEUE_ADD command to the muzykaPlayer with a given QueueEntry object.
	# This will add a QueueEntry to the Players queue.
	# 
	# @param queueEntry A queueEntry object
	# @return true if a valid response has been received after
	# executing the QUEUE_ADD command
	# @return false otherwise
	def addQueueItem(self,queueEntry):
		if queueEntry.entryType == QueueEntry.TYPE_FILE:
			mf = muzykaFrame( muzykaProtocol.QUEUE, muzykaProtocol.QUEUE_ADD,
			yaml.dump({'type': 'file', 'path': queueEntry.reference}))
		elif queueEntry.entryType == QueueEntry.TYPE_URL:
			mf = muzykaFrame( muzykaProtocol.QUEUE, muzykaProtocol.QUEUE_ADD,
			yaml.dump({'type': 'url', 'path': queueEntry.reference}))
		elif queueEntry.entryType == QueueEntry.TYPE_SONG:
			mf = muzykaFrame( muzykaProtocol.QUEUE, muzykaProtocol.QUEUE_ADD,
			yaml.dump({'type': 'song', 'song_id': queueEntry.reference}))
		else:
			return False
			
		response = self.sendMuzykaFrame(mf)
		
		if response.payload[0] == muzykaProtocol.SERVER_ACK:
			return True
		else:
			return False
		
	# Sends the QUEUE_LIST command to the muzykaPlayer.
	# 
	# @return a list of queue entries.
	def listQueue(self):
		mf = muzykaFrame( muzykaProtocol.QUEUE, muzykaProtocol.QUEUE_LIST, "")
		response = self.sendMuzykaFrame(mf)
		
		return yaml.load( response.payload )
		
		
	# Sends the PLAYER_STATUS command to the muzykaPlayer.
	# This will be used to get the currentQueueEntry, the elapsed time during audio playback, etc.
	# 
	# @return a dictionary according to the PLAYER_STATUS definition in muzykaProtocol/__init__.py
	def playerStatus(self):
		mf = muzykaFrame( muzykaProtocol.PLAYER, muzykaProtocol.PLAYER_STATUS, "")
		response = self.sendMuzykaFrame(mf)

		return yaml.load( response.payload )

	# Sends the QUEUE_REMOVE command to the muzykaPlayer to remove a single QueueEntry.
	# 
	# @param id The queue entry at <id> in the players queue will be deleted.
	# @return true if a valid response has been received after
	# executing the QUEUE_REMOVE command
	# @return false otherwise
	def removeQueueEntry(self,id):
		"""remove a single queue entry"""
		mf = muzykaFrame( muzykaProtocol.QUEUE, muzykaProtocol.QUEUE_REMOVE, int(id))
		
		response = self.sendMuzykaFrame(mf)
		if response.payload[0] == muzykaProtocol.SERVER_ACK:
			return True
		else:
			return False
			
	# Sends the QUEUE_SET_CURRENT_QUEUE_ENTRY command to the muzykaPlayer.
	# 
	# @param id The queue entry at <id> in the players queue will be the 'current queue entry'
	# 					When the player is in STOP state and receives the PLAY command after QUEUE_SET_CURRENT_QUEUE_ENTRY,
	#						the muzykaPlayer will play the song at 'current queue entry' next.
	# @return true if a valid response has been received after
	# executing the PLAY command
	# @return false otherwise
	def setCurrentQueueEntry(self,id):
		"""remove a single queue entry"""
		mf = muzykaFrame( muzykaProtocol.QUEUE, muzykaProtocol.QUEUE_SET_CURRENT_QUEUE_ENTRY, int(id))
  
		response = self.sendMuzykaFrame(mf)
		
		if response.payload[0] == muzykaProtocol.SERVER_ACK:
			return True
		else:
			return False
		
		