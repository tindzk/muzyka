# This Class will handle the communication between the actual server
# and the VLC-library, which will be used for audio playback.
# The VLC-library is accessed via vlcBindings.
# See http://wiki.videolan.org/Python_bindings for details.

# TODO: Implement the playback of QueueEntrys. This also includes
# 			the correct handling of the currentQueueEntry Reference.
# TODO: Test TYPE_URL Handling
# TODO: Handle current queue entry on QUEUE_REMOVE. Maybe the queue entry
# 			which is referenced by 'current queue entry' will be deleted.

from vlcBindings import *
from QueueEntry import *
import time
import yaml

class PlayerControl:
	# List of allowed audio file suffixes
	# This variable will be used to search for audio files
	# when adding a whole directory to the queue
	allowedAudioSuffix = ["mp3"]
	
	# Audio playback can be disabled for unit tests
	audioPlayback = True
	
	STATE_STOP = 0
	STATE_PLAYING = 1
	STATE_PAUSED = 2

	def __init__(self, audioPlayback = True):
		"""Constructor for PlayerControl Class"""
		self.shuffleMode = False
		self.milliSecondsPlayed = 0
		self.queue = []
		self.state = self.STATE_STOP
		self.currentQueueEntry = None
		# VLC player object
		self.player = None
		self.audioPlayback = audioPlayback
		
	# This method returns a representation of the player state as string
	@staticmethod
	def stateToString(state):
		if state == PlayerControl.STATE_STOP:
			return "STOP"
		elif state == PlayerControl.STATE_PLAYING:
			return "PLAYING"
		elif state == PlayerControl.STATE_PAUSED:
			return "PAUSED"
		else:
			raise NotImplemented
		
	# Get the elapsed seconds of the playback
	def getSecondsPlayed(self):
		return self.milliSecondsPlayed / 1000

	# Return the important PlayerControl variables as yaml string
	# This will be used for PLAYER_STATUS commands
	def playerStateToYAML(self):
		stateInYaml = {'state': self.state,
		 						'cqe': self.currentQueueEntry,
		 						'secPlayed': self.getSecondsPlayed()}
		
		# If the player hasnt been initialized before, we return 0 as duration
		if self.player == None:
			stateInYaml['duration'] = 0
		else:
		 	stateInYaml['duration'] = self.player.get_length() / 1000
			
		return yaml.dump(stateInYaml)
		
	def stop(self):
		"""Stops the current playback. Has no effect when the player is already in STOP state"""
		print "Called stop()"
		if self.state == self.STATE_PLAYING or self.state == self.STATE_PAUSED:
			if self.audioPlayback:
				print "Stopped Player"
				self.player.stop()
				print "Finished stop"
				
			# print "Finished if, set STOP state"
			self.milliSecondsPlayed = 0
			self.state = self.STATE_STOP
		# print "muzykaPlayer stop() called"
		
	# Initialize VLC and play a given file
	def initVLCWithFile(self,filename):
		audio = os.path.expanduser(filename)
		if not os.access(audio, os.R_OK):
			print('Error: %s file not readable' % audio)
			sys.exit(1)
		
		instance = Instance()
		try:
			media = instance.media_new(audio)  # load marqee option
		except NameError:
			print('NameError: %s (%s vs LibVLC %s)' % (sys.exc_info()[1],
                                                 __version__,
                                                 libvlc_get_version()))
			print "Exiting Server ..."
			sys.exit(1)
		
		self.player = instance.media_player_new()
		self.player.set_media(media)
		
	# Initialize VLC and play a given network stream
	def initVLCWithUrl(self,url):
		instance = Instance()
		try:
		    media = instance.media_new_location(url)  # load marqee option
		except NameError:
		    print('NameError: %s (%s vs LibVLC %s)' % (sys.exc_info()[1],
		                                               __version__,
		                                               libvlc_get_version()))
		    sys.exit(1)
		self.player = instance.media_player_new()
		self.player.set_media(media)

	def play(self):
		"""Plays the song at the currentQueueEntry position"""
		if self.currentQueueEntry == None:
			return
		
		# If there is no current song, load the next song and play it
		if self.state == self.STATE_STOP:
			# Get the queueEntry to play
			queueItem = self.queue[self.currentQueueEntry]
			
			# Proceed if the user wants audio playback
			if self.audioPlayback:
				if queueItem.entryType == QueueEntry.TYPE_FILE:
					self.initVLCWithFile(queueItem.reference)
				elif queueItem.entryType == QueueEntry.TYPE_URL:
					self.initVLCWithUrl(queueItem.reference)
				else:
					print "Unsupported QueueItem cannot be played!"
					return
					
				self.player.play()

				event_manager = self.player.event_manager()
				event_manager.event_attach(EventType.MediaPlayerEndReached, self.end_callback)
				event_manager.event_attach(EventType.MediaPlayerTimeChanged, self.time_update)
				
		elif self.state == self.STATE_PAUSED:
			if self.audioPlayback:
				self.player.pause()
		
		self.state = self.STATE_PLAYING
		
		# execute VLC play command
		
		print "muzykaPlayer start called"
		
	def pause(self):
		"""Pauses the current song """
		if self.state == self.STATE_PLAYING:
			if self.audioPlayback:
				self.player.pause()
			self.state = self.STATE_PAUSED
		elif self.state == self.STATE_PAUSED:
			# Awake the player
			if self.audioPlayback:
				self.player.pause()
			self.state = self.STATE_PLAYING
		
	# Increment the currentQueueEntry and begin with the
	# playback of the selected QueueEntry
	def next(self):
		# print "Called next()"
		if self.currentQueueEntry == None:
			return
		
		# print "List"
		queueLength = len(self.queue)
		# print "STATE IS " + str(self.state)
		# Set the currentQueueEntry to 0 if you call next() 
		# on the last currentQueueEntry
		if self.currentQueueEntry == queueLength-1:
			self.currentQueueEntry = 0
		else:
			self.currentQueueEntry+=1
			
		if self.state != self.STATE_STOP:
			print "Call stop and play with cqe" + str(self.currentQueueEntry)
			self.stop()
			print "In next goto play()"
			self.play()
		
	# Decrease the currentQueueEntry and begin with the
	# playback of the selected QueueEntry
	def prev(self):
		if self.currentQueueEntry == None:
			return
  
		queueLength = len(self.queue)
  
		# Set the currentQueueEntry to queueLength-1 if you call prev()
		# on the the first currentQueueEntry
		if self.currentQueueEntry == 0:
			self.currentQueueEntry = queueLength-1
		else:
			self.currentQueueEntry-=1
  
		if self.state != self.STATE_STOP:
			self.stop()
			self.play()
	
	# Add a given QueueEntry to the players queue
	def addQueueItem(self,item):
		"""Adds a item to the end of the queue"""
		if len(self.queue) == 0:
			self.currentQueueEntry = 0
		self.queue.append(item)
		
	# Add a list of QueueEntry Objects to the players queue
	def addListOfQueueItems(self,itemList):
		"""Adds a item to the end of the queue"""
		if len(self.queue) == 0 and len(itemList) > 0:
			self.currentQueueEntry = 0
			
		for item in itemList:
			self.queue.append(item)
		
	# Add a given QueueEntry to the players queue	at position <idx>
	def insertQueueItemAt(self,idx,item):
		"""Adds a item to position <idx> in the queue"""
		if len(self.queue) == 0:
			self.currentQueueEntry = 0
		self.queue.insert(idx,item)
		
	# Add a given List of QueueEntry Objects to the players queue	at position <idx>
	def insertQueueItemListAt(self,idx,itemList):
		"""Adds all QueueItems in itemList to the queue at <id>"""
		if len(self.queue) == 0 and len(itemList) > 0:
			self.currentQueueEntry = 0
		itemList.reverse()
		for item in itemList:
			self.queue.insert(idx,item)
		
	# Get the current queue of the player as list of QueueEntry Objects.
	def getQueue(self):
		"""Returns a list of all queue items"""
		return self.queue
				
	# Remove a QueueEntry at <idx>
	def removeQueueItem(self, idx):
		if idx < len(self.queue):
			self.queue.pop(idx)
			if len(self.queue) == 0:
				self.currentQueueEntry = None
			return True
		else:
			return False
			
	# Delete all QueueEntry Objects in the players queue.
	def flushQueue(self):
		"""Delete all entries from the queue"""
		self.stop()
		self.queue = []
	
	# Set the reference of the QueueEntry, which should be played next.
 	def setCurrentQueueEntry(self, itemIdx):
 		"""docstring for setCurrentQueueEntry"""
 		if itemIdx < len(self.queue):
 			self.currentQueueEntry = itemIdx
 			return True
 		else:
 			return False


	# ======================
	# = VLC handling stuff =
	# ======================
	
	# This method handles the callback of the vlc-library
	# when a audio file has been played completely. 
	def end_callback(self, event):
		print('End of media stream (event %s)' % event.type)
		self.state = self.STATE_STOP
		if self.currentQueueEntry == len(self.queue) -1:
			self.milliSecondsPlayed = 0
			print "Player finished queue and is now in STOP state"
		else:
			# print "Next song ..."
			self.next()
			self.play()
			
	# This method is called whenever the time in the vlc player has been changed
	def time_update(self, event):
		"""This callback method updates the local milliSecondsPlayed variable"""
		# print "Tick"
		self.milliSecondsPlayed = self.player.get_time()
