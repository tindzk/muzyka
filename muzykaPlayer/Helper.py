# This file has some helper methods for general use
def insertListItems(i, l, itemsToAdd):
	"""Inserts all items in itemsToAdd to list at index i """
	itemsToAdd.reverse()
	for item in itemsToAdd:
		l.insert(i, item)
	
	return l
	
# Takes a given amount of seconds and returns a
# string in the format of h:m:s
# If the seconds are below 3600, we wil only return m:s
def returnHourMinuteSecondString(seconds):
	m, s = divmod(seconds, 60)
	h, m = divmod(m, 60)
	
	if seconds<3600:
		return "%02d:%02d" % (m, s)
	else:
		return "%d:%02d:%02d" % (h, m, s)
	
# someList = [0,1]
# items = [2,3]
# 
# print insertListItems(1,someList,items)

# print returnHourMinuteSecondString(3600)