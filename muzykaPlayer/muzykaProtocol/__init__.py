# ========================
# = Protocol definitions =
# ========================
# The muzyka applications communicate via so called
# "muzykaFrames". These frames are structered like this:
# 
# | MFS | COMMANDGROUP | SUBCOMMAND | (payload/arguments)     | MFE |
# 1.Byte  2. Byte  				3. Byte     4-n-1. Byte             Last Byte (n)
# 
# The COMMANDGROUP describes the rough area where the command will be executed.
# For example: PLAYER, QUEUE, etc.
# 
# The SUBCOMMAND is the actual command in a given COMMANDGROUP.
# For example: PLAYER_PLAY in the COMMANDGROUP 'PLAYER' which starts the
# playback of the player.
# 
# The payload/arguments depends on the concrete COMMANDGROUP and SUBCOMMAND
# See the command definitions below for details
# 
# 
# The server (muzykaPlayer) will receive the muzyka frames and process them.
# Every client will then receive a response from the server with the 
# requested information or just some ACK/NACK flags.
# See the following command definitions for more details

# TODO:
# 	PLAYLIST GET ALL
# 	PLAYLIST GET ALL SONGS IN <playlist_id>
# 	PLAYLIST ADD SONG <playlist_id> <song_id>
# 	PLAYLIST REMOVE SONG <playlist_id> <song_id>





# muzyka frame start
MFS = '\x10'
# muzyka frame end
MFE = '\x11'

# ============================
# = Server response Payloads =
# ============================
#
# These constants will be returned by the server after some client requests.
# Further description is provided in the other constant definitions.

# Simple responses from the Server will only be "ACK"
SERVER_ACK = '\x12'

# 'Playlist not found' constant
PLAYLIST_NOT_FOUND = '\x20'

# 'Song not found' constant
SONG_NOT_FOUND = '\x21'

# 'Queue entry not found' constant
QUEUE_ENTRY_NOT_FOUND = '\x22'

# 'File not found' constant
FILE_NOT_FOUND = '\x23'

# 'Invalid frame' constant
INVALID_FRAME = '\x24'

# 'Directory not found' constant
# DIRECTORY_NOT_FOUND = '\x25'


# =================
# = Commandgroups =
# =================
PLAYLIST	=	'\x20'
QUEUE			=	'\x21'
PLAYER		=	'\x22'
SONG			= '\x23'

# ===============
# = Subcommands =
# ===============

# Subcommands for Playlist

# CREATE <listname>
# with listname as String
# Response from Server should be SERVER_ACK followed by 1 byte with the <id> of the created Playlist
PLAYLIST_CREATE		=	'\x01'

# DELETE <playlist_id>
# Response from Server should be the PLAYLIST DELETE SERVER_ACK (as payload)
# Response from Server should be PLAYLIST_NOT_FOUND as Payload when no playlist has been found with that id.
PLAYLIST_DELETE		=	'\x02'

# INFO <playlist_id>
# Response from Server should be a YAML formatted payload with the playlist information
# Response from Server should be PLAYLIST_NOT_FOUND as Payload when no playlist has been found with that id.
PLAYLIST_INFO		=	'\x03'



# Subcommands for Player

# PLAY
# Starts the playback of the queue from the current position or a paused song.
# Has no effect on empty queues on the player.
# Response from Server should be SERVER_ACK
PLAYER_PLAY	=	'\x01'

# STOP
# Stops the playback of the queue.
# Has no effect when no song is currently playing.
# Response from Server should be SERVER_ACK
PLAYER_STOP		=	'\x02'

# PAUSE
# Holds the playback of the queue.
# Has no effect when no song is currently playing.
# Response from Server should be SERVER_ACK
PLAYER_PAUSE	=	'\x03'

# NEXT
# Chooses the next QueueEntry in the players queue.
# If the player is currently playing a audio file,
# it will stop immediately and plays the next QueueEntry.
# Response from Server should be SERVER_ACK
PLAYER_NEXT	=	'\x04'

# PREV
# Chooses the previous QueueEntry in the players queue.
# If the player is currently playing a audio file,
# it will stop immediately and plays the previous QueueEntry.
# Response from Server should be SERVER_ACK
PLAYER_PREV	=	'\x05'

# STATUS
# Request the status of the queue. This constains:
# - secPlayed (the already played seconds of the current song. Is 0 if no song is currently playing)
# ( - song_id (the current song) )
# - queue_entry_id (the pointer to the current queue entry, e.g. song to be played). 
#                   Is Null if there are no entries in the queue.
# - duration: The length of the current queue entry in seconds
# Response from Server should be a list of the described queue entries in YAML format.
# 
# {'state': 0, 'cqe': 1, 'secPlayed': 23, 'duration': 123}
PLAYER_STATUS	=	'\x06'


# Subcommands for Queue


# ADD <params_in_yaml>
# Adds a given entry to the players queue
# The type of entry can be:
# * song
#   In this case you should pass a dictionary to yaml.dump() with the following structure:
#   {'type': 'song', 'song_id': 5}
# 
# * playlist
#   TODO: YAML definition
# 
# * file
#   In this case you should pass a dictionary to yaml.dump() with the following structure:
#   {'type': 'file', 'path': '/tmp/nyan.mp3'}
#   Note: You can also add a whole directory with this command if you pass a path to a directory.
#   muzykaPlayer will then look for any files with an suffix listed in PlayerControl.allowedAudioSuffix
#   and adds them to the queue.
# 
# * network stream
# 	{'type': 'url', 'path': 'http://207.200.96.229:8030'}
# 
# Response from Server should be the SERVER_ACK followed by 1 byte with the <entry_queue_id> if the song has been found and added.
# Response from Server should be SONG_NOT_FOUND if the given song id has not been found.
# Response from Server should be FILE_NOT_FOUND if the given filepath is not existing on the host
# Response from Server should be PLAYLIST_NOT_FOUND if the given playlist id has not been found.
# Response from Server should be INVALID_FRAME if the given YAML payload is not valid.
QUEUE_ADD	=	'\x01'


# REMOVE <queue_entry_id>
# Removes a given song id to the players queue
# Response from Server should be SERVER_ACK if song has been found and removed.
# Response from Server should be QUEUE_ENTRY_NOT_FOUND if the given queue entry has not been found.
QUEUE_REMOVE	=	'\x02'

# LIST
# Request all queue entries in the muzykaPlayer queue
# Response from Server should be a list of queue entries in YAML format.
QUEUE_LIST	=	'\x03'

# INSERT_SONG_AT <params_in_yaml>
# 
# Adds a given entry to the players queue at a given position
# This directly maps to list.insert() described here:
# http://docs.python.org/tutorial/datastructures.html
# 
# The type of entry can be:
# * song
#   In this case you should pass a dictionary to yaml.dump() with the following structure:
#   {'type': 'song', 'song_id': 5, 'idx': 0}
# 	where idx is the desired position of the item in the queue.
# 
# * playlist
#   TODO: YAML definition
# 
# * file
#   In this case you should pass a dictionary to yaml.dump() with the following structure:
#   {'type': 'file', 'path': '/tmp/nyan.mp3', 'idx': 0}
# 	where idx is the desired position of the item in the queue.
#   Note: You can also add a whole directory with this command if you pass a path to a directory.
#   muzykaPlayer will then look for any files with an suffix listed in PlayerControl.allowedAudioSuffix
#   and adds them to the queue.
# 
# * network stream
# 	{'type': 'url', 'path': 'http://207.200.96.229:8030', 'idx': 0 }
#
# 
# Response from Server should be the SERVER_ACK followed by 1 byte with the <entry_queue_id> if the song has been found and added.
# Response from Server should be SONG_NOT_FOUND if the given song id has not been found.
# Response from Server should be FILE_NOT_FOUND if the given filepath is not existing on the host
# Response from Server should be PLAYLIST_NOT_FOUND if the given playlist id has not been found.
# Response from Server should be INVALID_FRAME if the given YAML payload is not valid.
QUEUE_INSERT_AT	=	'\x05'

# SET_CURRENT_QUEUE_ENTRY <id>
# 
# Sets the current queue entry to a specific number.
# 
# Response from Server should be the SERVER_ACK if the desired queue entry is existing and can be set as 'current queue entry'
# Response from Server should be QUEUE_ENTRY_NOT_FOUND if the given queue entry has not been found.
QUEUE_SET_CURRENT_QUEUE_ENTRY = '\x06'