# This class models the structure of a muzykaFrame.
# The frames contain a COMMANDGROUP, a SUBCOMMAND and a PAYLOAD.
# These terms are described in muzykaProtocol/__init__.py

import muzykaProtocol

class muzykaFrame:
	commandgroup = ''
	subcommand = ''
	payload = ""
	
	def __init__(self,commandgroup=None,subcommand=None,payload=None):
		if subcommand is None and payload is None and commandgroup is not None:
			# If only the first argument is given, we will parse this input as a frame
			self.parseFrame(commandgroup)
		else:
			self.commandgroup = commandgroup
			self.subcommand = subcommand
			self.payload = str(payload)
		
	def toString(self):
		return muzykaProtocol.MFS + self.commandgroup + self.subcommand + self.payload + muzykaProtocol.MFE
		
	def parseFrame(self,frame):
		if frame[0] == muzykaProtocol.MFS and frame[-1] == muzykaProtocol.MFE:
			self.commandgroup	= frame[1]
			self.subcommand		= frame[2]
			self.payload	=	frame[3:-1]
		else:
			raise "Invalid frame received but requested to be processed " + frame
		
	@staticmethod
	def debugString(frameAsString):
		i=0
		for c in frameAsString:
			print "%#x" % ord(c),
			if i == 0 and c == muzykaProtocol.MFS:
				print "(Frame start)"
			elif i == 1:
				print "(Commandgroup)"
			elif i == 2:
				print "(Subcommand)"
			elif i == 3:
				print "(Payload begin)"
			elif (i+1) == len(frameAsString) and c == muzykaProtocol.MFE:
				print "(Frame end)"
			else:
				print "" # Draw newline
			i+= 1
		