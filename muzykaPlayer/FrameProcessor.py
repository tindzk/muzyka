# This class is an abstract definition for frame processors.
# The derived classes should provide a process() method which will
# check a given frame, trigger the necessary actions and then return a
# response in form of a muzykaFrame

class FrameProcessor:
	playerControl = None

	# @staticmethod
	def process(self,receivedFrame):
		"""Abstract definition of process()"""
		# print "abstract static"
		raise NotImplementedError	