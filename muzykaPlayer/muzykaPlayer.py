# This class implements a server for the muzyka protocol.
# See https://bitbucket.org/tindzk/muzyka/
# It listens on a UNIX Socket and receives "muzyka frames".
# These frames will be processed by *FrameProcessor which will handle
# all the possible incoming frames. The *FrameProcessors will also execute
# the desired commands (like PLAY, STOP, ETC. ) which have been received 
# over the socket


# TODO: Handle LARGE packets with more than 16384 bytes and splitted frames
# TODO: Exception handling for the *FrameProcessor response

import socket,os
import muzykaProtocol
from muzykaProtocol.muzykaFrame import muzykaFrame
from PlayerControl import PlayerControl
from PlayerFrameProcessor import PlayerFrameProcessor
from QueueFrameProcessor import QueueFrameProcessor


# Process a received framed with the coressponding FrameProcessor Classes
# @param frame The received frame as string
# @return False if the COMMANDGROUP in the frame is not implemented by a
# 				*FrameProcessor
# @return a muzykaFrame Object with the response if the incoming frame has 
#					 been handled by a *FrameProcessor.
def processFrame(frame):
	"""This method receives all the incoming frames"""
	
	receivedFrame = muzykaFrame(frame)
	if receivedFrame.commandgroup == muzykaProtocol.PLAYER:
		return playerFrameProcessor.process(receivedFrame)
	elif receivedFrame.commandgroup == muzykaProtocol.QUEUE:
		return queueFrameProcessor.process(receivedFrame)
	# 	processQueueFrame(receivedFrame)
	# elif receivedFrame.commandgroup == muzykaProtocol.PLAYLIST:
	# 	processPlaylistFrame(receivedFrame)		
	else:
		print "processFrame doesnt know how to handle commandgroup " + ("%#x" % ord(receivedFrame.commandgroup))
		return False

# ======================
# = Begin main program =
# ======================	


# Define a player control object and the frame processors
playerControl = PlayerControl()
playerFrameProcessor = PlayerFrameProcessor(playerControl)
queueFrameProcessor = QueueFrameProcessor(playerControl)
	
s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
try:
	os.remove("/tmp/muzyka.sock")
except OSError:
	pass
s.bind("/tmp/muzyka.sock")
print "Listening..."
s.listen(1)
print "Accepting ..."
while 1:
	conn, addr = s.accept()
	print "Accepted!"

	while 1:
		data = conn.recv(16384)
		if not data:	break
		if data[0] == muzykaProtocol.MFS and data[-1] == muzykaProtocol.MFE:
			print "Received valid frame!"
			responseFrame = processFrame(data)
			# TODO: Implement exception handling
			if responseFrame == False:
				conn.close()
				print "The packet could not be handled by processFrame - closing connection ..."
				break
		else:
			print "Received invalid frame - Closing connection"
			conn.close()			
		conn.send(responseFrame.toString())
	print "Closing Connection"
	conn.close()