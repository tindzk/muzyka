# This class defines methods to process a QUEUE-related frame
# It will check a given frame, trigger the necessary actions and then return a
# response in form of a muzykaFrame

# TODO: What happens when someone adds an empty directory?

from FrameProcessor import FrameProcessor
from muzykaProtocol.muzykaFrame import muzykaFrame
import muzykaProtocol
import yaml
from struct import *
import QueueEntry
from QueueEntry import QueueEntry
import os.path
# import helper

class QueueFrameProcessor(FrameProcessor):
	def __init__(self, playerControl):
		"""docstring for __init__"""
		self.playerControl = playerControl
		
	# Fetch all files in the given _directory_, which have a PlayerControl.allowedAudioSuffix
	# and add them to the queue.
	# The directory in path must be existing.
	def addAudioFilesInDirectoryToQueue(self, path , idx = None):
		# Create a list of QueueEntries for all audio files
		dirList = os.listdir(path)
		audioFiles = []
		
		for filename in dirList:
			fileSuffix = filename.split(".")[-1]
			
			# is the suffix in the list of allowed audio suffixes?
			if self.playerControl.allowedAudioSuffix.count(fileSuffix):
				fileItem = QueueEntry( QueueEntry.TYPE_FILE, os.path.join(path, filename) )
				audioFiles.append(fileItem)
			else:
				break
		
		# If no idx is given, the list will be appended at the end of the queue.
		# Otherwise, it will be inserted at idx.
		if idx == None:		
			self.playerControl.addListOfQueueItems(audioFiles)
		else:
			self.playerControl.insertQueueItemListAt(idx, audioFiles)
			
		
	
	# This methods processes a given frame and takes the necessary actions
	# @param receivedFrame a muzykaFrame Object
	# @return a muzykaFrame Object with the response
	def process(self,receivedFrame):
		"""Frame handling for QUEUE-related frames"""
		responseFrame = muzykaFrame()
		
		# Answer with the same commandgroup and subcommand
		responseFrame.commandgroup	=	receivedFrame.commandgroup
		responseFrame.subcommand		=	receivedFrame.subcommand
		
		# ===========================
		# = Handle QUEUE ADD frames =
		# ===========================
		
		if receivedFrame.subcommand == muzykaProtocol.QUEUE_ADD:
			# Decode the received payload from yaml to a dictionary
			payload = yaml.load(receivedFrame.payload)
			
			# Add the item to the queue and save the items position
			queueId = len(self.playerControl.getQueue())
			
			# Decide the next steps due to the items type
			if payload['type'] == 'song':
				
				songItem = QueueEntry( QueueEntry.TYPE_SONG, int(payload['song_id']))
				self.playerControl.addQueueItem( songItem )
				
				responseFrame.payload 			= muzykaProtocol.SERVER_ACK + str(queueId)
				# Response from Server should be SONG_NOT_FOUND if the given song has not been found.
				# TODO: Check if the given song id is in our Database			
				
			elif payload['type'] == 'file':
				# Check the presence of the given file at the filepath
				if os.path.isfile(payload['path']):
					# The file has been found locally. add it to the list
					fileItem = QueueEntry( QueueEntry.TYPE_FILE, payload['path'])
					self.playerControl.addQueueItem( fileItem )
			
					responseFrame.payload 			= muzykaProtocol.SERVER_ACK + str(queueId)
				else:
					# When its not a file, its maybe a directory
					if os.path.isdir(payload['path']):
						self.addAudioFilesInDirectoryToQueue(payload['path'])
						
						responseFrame.payload 			= muzykaProtocol.SERVER_ACK + str(queueId)
					else:
						responseFrame.payload 			= muzykaProtocol.FILE_NOT_FOUND
				
			elif payload['type'] == 'url':
				if not payload['path'][0:7] == "http://":
					responseFrame.payload 			= muzykaProtocol.INVALID_FRAME
					return responseFrame
					
				urlItem = QueueEntry( QueueEntry.TYPE_URL, payload['path'])
				self.playerControl.addQueueItem( urlItem )
			
				responseFrame.payload 			= muzykaProtocol.SERVER_ACK + str(queueId)
			else:
				responseFrame.payload 			= muzykaProtocol.INVALID_FRAME
			
			return responseFrame
			
		# ==============================
		# = Handle QUEUE REMOVE frames =
		# ==============================
		
		elif receivedFrame.subcommand == muzykaProtocol.QUEUE_REMOVE:
			
			queueLength = len(self.playerControl.getQueue())
			if abs(int(receivedFrame.payload)) >= queueLength:
				# Respond with QUEUE_ENTRY_NOT_FOUND if the given index is out of bounds.
				responseFrame.payload = muzykaProtocol.QUEUE_ENTRY_NOT_FOUND
				return responseFrame
			else:
				self.playerControl.removeQueueItem( int(receivedFrame.payload) )
				responseFrame.payload = muzykaProtocol.SERVER_ACK
				return responseFrame
				
 		# ============================
 		# = Handle QUEUE LIST frames =
 		# ============================
				
		elif receivedFrame.subcommand == muzykaProtocol.QUEUE_LIST:
			# Pack the current queue as a list in YAML
			
			responseFrame.payload = yaml.dump(self.playerControl.getQueue())
			return responseFrame
			
		# =================================
		# = Handle QUEUE INSERT AT frames =
		# =================================
		elif receivedFrame.subcommand == muzykaProtocol.QUEUE_INSERT_AT:
			payload = yaml.load(receivedFrame.payload)
			insertAtIdx = abs( int(payload['idx']) )
			
			queueId = len(self.playerControl.getQueue())
			
			# TODO: Handle negative values correctly
			insertAtIdx = abs(insertAtIdx)
			
			# Check if the received index is out of bounds.
			if insertAtIdx > queueId:
				insertAtIdx = queueId
			
			# Decide the next steps due to the items type
			
			if payload['type'] == 'song':
				# Response from Server should be SONG_NOT_FOUND if the given song has not been found.
				# TODO: Check if the given song id is in our Database			
				
				songItem = QueueEntry( QueueEntry.TYPE_SONG, int(payload['song_id']))
				self.playerControl.insertQueueItemAt( insertAtIdx, songItem )

				responseFrame.payload 			= muzykaProtocol.SERVER_ACK + str(insertAtIdx)
				
			elif payload['type'] == 'file':
				# Check the presence of the given file at the filepath
				if os.path.isfile(payload['path']):
					fileItem = QueueEntry( QueueEntry.TYPE_FILE, payload['path'])			
					self.playerControl.insertQueueItemAt( insertAtIdx, fileItem )
					
					responseFrame.payload 			= muzykaProtocol.SERVER_ACK + str(insertAtIdx)
				else:
					# When its not a file, its maybe a directory
					if os.path.isdir(payload['path']):
						self.addAudioFilesInDirectoryToQueue(payload['path'],insertAtIdx)
						
						responseFrame.payload 			= muzykaProtocol.SERVER_ACK + str(insertAtIdx)
					else:
						responseFrame.payload 			= muzykaProtocol.FILE_NOT_FOUND
			elif payload['type'] == 'url':
				if not payload['path'][0:7] == "http://":
					responseFrame.payload 			= muzykaProtocol.INVALID_FRAME
					return responseFrame
      
				urlItem = QueueEntry( QueueEntry.TYPE_URL, payload['path'])
				self.playerControl.insertQueueItemAt( insertAtIdx, urlItem )
      
				responseFrame.payload 			= muzykaProtocol.SERVER_ACK + str(insertAtIdx)
			else:
				responseFrame.payload 			= muzykaProtocol.INVALID_FRAME
				
			return responseFrame
		
		# ========================================
		# = Handle QUEUE SET CURRENT QUEUE ENTRY =
		# ========================================
		
		elif receivedFrame.subcommand == muzykaProtocol.QUEUE_SET_CURRENT_QUEUE_ENTRY:
			queueLength = len(self.playerControl.getQueue())
			cqe = abs(int(receivedFrame.payload))
			
			if cqe >= queueLength:
				# Respond with QUEUE_ENTRY_NOT_FOUND if the given index is out of bounds.
				responseFrame.payload = muzykaProtocol.QUEUE_ENTRY_NOT_FOUND
				return responseFrame
			else:
				self.playerControl.currentQueueEntry = cqe
				responseFrame.payload = muzykaProtocol.SERVER_ACK
				return responseFrame

		# ===============================
		# = Unknown SUBCOMMAND handling =
		# ===============================
		else:
			print "QueueFrameProcessor doesnt know how to handle subcommand " + ("%#x" % ord(receivedFrame.subcommand))
			raise NotImplementedError
		
