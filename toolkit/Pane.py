from Toolkit import *

surface = Surface(800, 400)
window  = GtkWindow(surface)

box = Box()
box.setColor((0.5, 0.5, 0.5))

box2 = Box()
box2.setColor((0.8, 0.8, 0.8))

pane = VertPane()
pane.addColumn(0.25, box)
pane.addColumn(0.5,  box2)
pane.addColumn(0.25, box)

window.addWidget(pane, 0, 0)

window.render()
