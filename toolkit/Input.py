from Toolkit import *

surface = Surface(300, 300)
window  = GtkWindow(surface)

input = Input()
window.addWidget(input, 15, 15)

window.setActive(input)
window.render()