from Toolkit import *

surface = Surface(600, 200)
window  = GtkWindow(surface)

title  = Text("Ac ne quis a nobis hoc ita dici forte miretur.")
title2 = Text("Quod alia quaedam in hoc facultas sit ingeni.")

window.addWidget(title,  5,  5)
window.addWidget(title2, 15, 35)

window.addWidget(title,  80, 100)
window.addWidget(title2, 90, 110) # Overlay.

window.render()
