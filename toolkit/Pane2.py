from Toolkit import *

surface = Surface(800, 400)
window  = GtkWindow(surface)

title = Text("Ac ne quis a nobis hoc ita dici forte miretur.")
page  = Text("Track 1 of 249")

box = Box()
box.setColor((0.8, 0.8, 0.8))

pane = VertPane()
pane.addColumn(0.80, title)
pane.addColumn(0.01, box)
pane.addColumn(0.19, page)

pane2 = HorizPane()
pane2.addRow(0.1, pane)
pane2.addRow(0.8, box)
pane2.addRow(0.1, pane)

window.addWidget(pane2, 0, 0)

window.render()
