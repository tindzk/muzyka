import cairo
import pango
import pangocairo

class Keys:
	Up        = 0
	Down      = 1
	PgUp      = 2
	PgDown    = 3
	Return    = 4
	BackSpace = 5
	Textual   = 6

class Mouse:
	class PressType:
		Click       = 0
		DoubleClick = 1

	class Scroll:
		Up     = 0
		Down   = 1
		Left   = 2
		Right  = 3

class Surface:
	w = -1
	h = -1
	cr = None

	def __init__(self, w, h):
		self.w = w
		self.h = h
		self.surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, w, h)
		self.cr = cairo.Context(self.surface)

	def getWidth(self):
		return self.w

	def getHeight(self):
		return self.h

	# See http://stackoverflow.com/questions/4889045/how-to-get-transparent-background-in-window-with-pygtk-and-pycairo
	def clear(self):
		# Sets the operator to clear which deletes everything below where an
		# object is drawn.
		self.cr.set_operator(cairo.OPERATOR_CLEAR)

		# Makes the mask fill the entire window.
		self.cr.rectangle(0.0, 0.0, self.w, self.h)

		# Deletes everything in the window (since the compositing operator is
		# `clear' and mask fills the entire window.
		self.cr.fill()

		# Set the compositing operator back to the default.
		self.cr.set_operator(cairo.OPERATOR_OVER)

	def getFonts(self):
		fontMap = pangocairo.cairo_font_map_get_default()
		families = self.fontMap.list_families()
		return [f.get_name() for f in families]

	def setSourceRgb(self, r, g, b):
		self.cr.set_source_rgb(r, g, b)

	def rectangle(self, x, y, w, h):
		assert x + w <= self.w
		assert y + h <= self.h

		self.cr.rectangle(x, y, h, w)

	def drawBorder(self, rgb = (1, 1, 1), width = 1):
		self.cr.stroke_preserve()
		self.cr.set_line_width(width)
		self.cr.set_source_rgb(*rgb)

	def fill(self):
		self.cr.fill()

class Canvas:
	class Orientation:
		Left   = 0
		Center = 1
		Right  = 2
		Top    = 3
		Bottom = 4

	x = -1
	y = -1
	h = -1
	w = -1
	surface = None
	cr = None
	parent = None
	children = []
	offset = (0, 0)

	def __init__(self, surface, x = 0, y = 0, w = -1, h = -1):
		if w == -1:
			w = surface.getWidth() - x

		if h == -1:
			h = surface.getHeight() - y

		self.x = x
		self.y = y
		self.h = h
		self.w = w

		assert x + w <= surface.getWidth()
		assert y + h <= surface.getHeight()

		self.surface = surface

		# @todo Use recording surface for better performance.
		# cf. http://cairographics.org/manual/cairo-Recording-Surfaces.html
		# self.recording = cairo.RecordingSurface(cairo.CONTENT_COLOR_ALPHA)

		self.recording = cairo.ImageSurface(cairo.FORMAT_ARGB32, int(w), int(h))
		self.cr = cairo.Context(self.recording)

	def inherit(parent, x = 0, y = 0, w = -1, h = -1):
		return Canvas(parent.surface, parent.x + x, parent.y + y, w, h)

	def crop(parent, x, y):
		return Canvas(parent.surface, parent.x + x, parent.y + y, parent.w - x, parent.h - y)

	def setParent(self, canvas):
		self.parent = canvas

	def getWidth(self):
		return self.w

	def getHeight(self):
		return self.h

	def drawBox(self, color):
		self.surface.setSourceRgb(*color)
		self.surface.rectangle(self.x, self.y, self.w, self.h)
		self.surface.drawBorder()
		self.surface.fill()

	def paintText(self, font, text, orientation):
		pangoCairoContext = pangocairo.CairoContext(self.cr)
		pangoCairoContext.set_antialias(cairo.ANTIALIAS_SUBPIXEL)

		font = pango.FontDescription(font)

		layout = pangoCairoContext.create_layout()
		layout.set_font_description(font)
		layout.set_text(text)

		pangoCairoContext.update_layout(layout)
		pangoCairoContext.show_layout(layout)

		size = layout.get_size()

		w = size[0] / pango.SCALE
		h = size[1] / pango.SCALE

		if orientation == self.Orientation.Left:
			self.offset = (0, 0)
		elif orientation == self.Orientation.Center:
			self.offset = ((self.w - w) / 2, 0)
		elif orientation == self.Orientation.Right:
			self.offset = (self.w - w, 0)
		elif orientation == self.Orientation.Top:
			self.offset = (0, 0)
		elif orientation == self.Orientation.Bottom:
			self.offset = (0, self.h - h)

	def render(self):
		self.surface.cr.save()
		self.surface.cr.translate(*self.offset)
		self.surface.cr.set_source_surface(self.recording, self.x, self.y)
		self.surface.cr.paint()
		self.surface.cr.restore()

		self.offset = (0, 0)

		for child in self.children:
			child.render()

class Window:
	class WidgetItem:
		widget = None
		x = -1
		y = -1

		def __init__(self, widget, x, y):
			self.widget = widget
			self.x = x
			self.y = y

	active  = None # Refers to the active widget.
	surface = None
	widgets = []

	def __init__(self, surface):
		self.surface = surface

	def addWidget(self, widget, x, y):
		self.widgets.append(self.WidgetItem(widget, x, y))

	def findNextActive(self):
		return None

	def setActive(self, widget):
		self.active = widget

	def getActive(self):
		return self.active

	def onKeyPress(self, key, value = ''):
		if self.active is None:
			return

		self.active.onKeyPress(key, value)

	def onScroll(self, dir):
		if self.active is None:
			return

		self.active.onScroll(dir)

	def onButtonPress(self, type, x, y):
		if self.active is None:
			return

		self.active.onButtonPress(type, x, y)

	def render(self):
		self.surface.clear()

		for elem in self.widgets:
			canvas = Canvas(self.surface, x = elem.x, y = elem.y)
			elem.widget.render(canvas)

import pygtk
import gtk, gobject

class GtkScreen(gtk.DrawingArea):
	# Draw in response to an `expose'-event.
	__gsignals__ = { "expose-event": "override" }

	wnd = None

	def setWindow(self, wnd):
		self.wnd = wnd

	def do_expose_event(self, event):
		assert self.wnd is not None

		cr = self.window.cairo_create()

		# Restrict Cairo to the exposed area.
		cr.rectangle(event.area.x, event.area.y, event.area.width, event.area.height)
		cr.clip()

		cr.set_operator(cairo.OPERATOR_CLEAR)
		cr.rectangle(0.0, 0.0, *self.window.get_size())
		cr.fill()
		cr.set_operator(cairo.OPERATOR_OVER)

		# Fill the background with white.
		cr.set_source_rgb(1, 1, 1)
		cr.rectangle(0, 0, *self.window.get_size())
		cr.fill()

		cr.set_source_surface(self.wnd.surface.surface)
		cr.paint()

class GtkWindow(Window):
	screenWidget = None

	def _onKeyPress(self, widget, event):
		key = event.keyval

		if key == gtk.keysyms.Up:
			self.onKeyPress(Keys.Up)
		elif key == gtk.keysyms.Down:
			self.onKeyPress(Keys.Down)
		elif key == gtk.keysyms.Page_Down:
			self.onKeyPress(Keys.PgDown)
		elif key == gtk.keysyms.Page_Up:
			self.onKeyPress(Keys.PgUp)
		elif key == gtk.keysyms.Return:
			self.onKeyPress(Keys.Return)
		elif key == gtk.keysyms.BackSpace:
			self.onKeyPress(Keys.BackSpace)
		else:
			ukey = unichr(gtk.gdk.keyval_to_unicode(event.keyval))

			if len(ukey) != 0:
				self.onKeyPress(Keys.Textual, ukey)

		# @todo Inefficient.
		Window.render(self)
		self.screenWidget.queue_draw()

	def _onScroll(self, widget, event):
		dir = event.direction

		if dir == gtk.gdk.SCROLL_UP:
			self.onScroll(Mouse.Scroll.Up)
		elif dir == gtk.gdk.SCROLL_DOWN:
			self.onScroll(Mouse.Scroll.Down)
		elif dir == gtk.gdk.SCROLL_LEFT:
			self.onScroll(Mouse.Scroll.Left)
		elif dir == gtk.gdk.SCROLL_RIGHT:
			self.onScroll(Mouse.Scroll.Right)
		else:
			return

		# @todo Inefficient.
		Window.render(self)
		self.screenWidget.queue_draw()

	def _onButtonPress(self, widget, event):
		type = Mouse.PressType.Click
		if event.type == gtk.gdk._2BUTTON_PRESS:
			type = Mouse.PressType.DoubleClick

		self.onButtonPress(type, event.x, event.y)

		# @todo Inefficient.
		Window.render(self)
		self.screenWidget.queue_draw()

	def render(self):
		Window.render(self)

		window = gtk.Window()

		window.add_events(
			gtk.gdk.POINTER_MOTION_MASK |
			gtk.gdk.BUTTON_PRESS_MASK   |
			gtk.gdk.KEY_PRESS_MASK      |
			gtk.gdk.SCROLL_MASK)

		window.set_default_size(self.surface.w, self.surface.h)

		window.connect("delete-event", gtk.main_quit)
		window.connect("key-press-event", self._onKeyPress)
		window.connect("scroll-event", self._onScroll)
		window.connect("button-press-event", self._onButtonPress)

		self.screenWidget = GtkScreen()
		self.screenWidget.setWindow(self)
		self.screenWidget.show()

		window.add(self.screenWidget)
		window.present()

		gtk.main()

class PngWindow(Window):
	path = "out.png"

	def setPath(self, path):
		self.path = path

	def show(self):
		Window.render(self)
		self.surface.write_to_png(self.path)

class Widget:
	def render(self, canvas):
		assert isinstance(Canvas, canvas)

	def onKeyPress(self, key, value):
		assert True

	def onScroll(self, dir):
		assert True

	def onButtonPress(self, type, x, y):
		assert True

class Pane(Widget):
	class Item:
		proportion = 0.0
		widget     = None

		def __init__(self, proportion, widget):
			self.proportion = proportion
			self.widget     = widget

	items = []

	def checkProportions(self):
		return sum(elem.proportion for elem in self.items) <= 1.0

	def _add(self, proportion, widget):
		assert proportion > 0
		assert widget is None or isinstance(widget, Widget)

		self.items.append(VertPane.Item(proportion, widget))

		assert self.checkProportions()

	def setBorder(self, width = 0):
		self.border = width

class VertPane(Pane):
	def __init__(self):
		self.items = [] # @todo Why needed?

	def addColumn(self, proportion, widget):
		self._add(proportion, widget)

	def render(self, canvas):
		ofs = 0

		for elem in self.items:
			width = elem.proportion * canvas.getWidth()
			if not elem.widget is None:
				subCanvas = Canvas.inherit(canvas, x = ofs, w = width)
				elem.widget.render(subCanvas)

			# @todo Draw separator.

			ofs += width

		canvas.render()

class HorizPane(Pane):
	def __init__(self):
		self.items = [] # @todo Why needed?

	def addRow(self, proportion, widget):
		self._add(proportion, widget)

	def render(self, canvas):
		ofs = 0

		for elem in self.items:
			height = elem.proportion * canvas.getHeight()

			subCanvas = Canvas.inherit(canvas, y = ofs, h = height)
			elem.widget.render(subCanvas)

			# @todo Draw separator.

			ofs += height

		canvas.render()

class Box(Widget):
	color = (0, 0, 0)

	def setColor(self, rgb):
		self.color = rgb

	def render(self, canvas):
		canvas.rectangle(self.color)
		canvas.render()

class List(Widget):
	class Item:
		widget = None

		def __init__(self, widget):
			self.widget = widget

	canvas       = None
	onSelect     = None
	items        = []
	offset       =  0
	active       =  0
	visible      = -1
	itemHeight   = 30
	itemsPerPage =  0

	def add(self, widget):
		self.items.append(self.Item(widget))

	def get(self, idx):
		assert idx == -1 or idx < len(self.items)
		return self.items[idx].widget

	def setActive(self, idx):
		assert idx == -1 or idx < len(self.items)
		self.active = idx

	def onKeyPress(self, key, value):
		if key == Keys.Up and self.active > 0:
			if self.active == self.offset:
				if self.offset > 0:
					self.offset -= 1

			self.active -= 1

		elif key == Keys.Down and self.active + 1 < len(self.items):
			if self.active + 1 == self.offset + self.visible:
				if self.offset + 1 < len(self.items):
					self.offset += 1

			self.active += 1

		elif key == Keys.PgUp and self.active > 0:
			ofs = self.itemsPerPage
			if ofs > self.active:
				ofs = self.active

			if self.offset > self.itemsPerPage:
				self.offset -= self.itemsPerPage
			else:
				self.offset = 0

			self.active -= ofs

		elif key == Keys.PgDown and self.active + 1 < len(self.items):
			ofs = self.itemsPerPage
			if ofs + self.active > len(self.items):
				ofs = len(self.items) - self.active

			if self.offset + ofs + self.itemsPerPage < len(self.items):
				self.offset += ofs
			else:
				self.offset = len(self.items) - self.itemsPerPage

			if self.active + ofs < len(self.items):
				self.active += ofs
			else:
				self.active = len(self.items) - 1

		elif key == Keys.Return:
			if self.active != -1:
				self.onSelect(self, self.active)

	def onScroll(self, dir):
		if dir == Mouse.Scroll.Up:
			self.onKeyPress(Keys.PgUp)

		elif dir == Mouse.Scroll.Down:
			self.onKeyPress(Keys.PgDown)

	def locateItem(self, x, y):
		if self.canvas.y < y < self.canvas.y + self.canvas.h:
			ofs  = y - self.canvas.y
			item = int(ofs / self.itemHeight)

			return self.offset + item

		return -1

	def onButtonPress(self, type, x, y):
		idx = self.locateItem(x, y)

		if idx != -1:
			self.active = idx

			if type == Mouse.PressType.DoubleClick:
				self.onSelect(self, idx)

	def setOnSelect(self, cb):
		self.onSelect = cb

	def render(self, canvas):
		self.canvas       = canvas
		self.visible      = int(canvas.getHeight() / self.itemHeight)
		self.itemsPerPage = self.visible

		if self.visible > len(self.items):
			self.visible = len(self.items)

		ofs = 0

		for i in range(self.offset, self.offset + self.visible):
			# Draw marker for active item.
			if i == self.active:
				marker = Canvas.inherit(canvas, x = 2, y = ofs + 5, w = 2, h = self.itemHeight - 5)
				marker.rectangle((0.8, 0.8, 0.8))
				marker.render()

			cnvWidget = Canvas.inherit(canvas, x = 5, y = ofs, h = self.itemHeight)
			self.items[i].widget.render(cnvWidget)

			ofs += self.itemHeight

			# @todo Draw separator, add spacing.

		canvas.render()

class Text(Widget):
	class Weight:
		Normal = 0
		Bold   = 1
		Italic = 2

	font        = "Linux Libertine O"
	text        = ""
	size        = 20
	weight      = Weight.Normal
	orientation = Canvas.Orientation.Left

	def __init__(self, text = "", font = "Linux Libertine O", weight = Weight.Normal, size = 20, orientation = Canvas.Orientation.Left):
		self.text   = text
		self.font   = font
		self.size   = size
		self.weight = weight
		self.orientation = orientation

	def setText(self, text):
		self.text = text

	def getText(self):
		return self.text

	def setSize(self, size):
		self.size = size

	def setFont(self, font):
		self.font = font

	def setWeight(self, weight):
		self.weight = weight

	def setOrientaton(self, orientation):
		self.orientation = orientation

	def render(self, canvas):
		font = self.font

		if self.weight == self.Weight.Bold:
			font += " Bold"
		elif self.weight == self.Weight.Italic:
			font += " Italic"

		font += " " + str(self.size)

		canvas.paintText(font, self.text, self.orientation)
		canvas.render()

class Input(Widget):
	text = None

	def __init__(self, text = Text()):
		self.text = text

	def setText(self, text):
		self.text = text

	def getText(self):
		return self.text

	def onKeyPress(self, key, value):
		if key == Keys.Textual:
			self.text.text += value

		elif key == Keys.BackSpace:
			if len(self.text.text) > 0:
				self.text.text = self.text.text[0:-1]

	def render(self, canvas):
		area = Canvas.crop(canvas, x = 1, y = 1)

		border = Canvas.crop(area, x = 1, y = 1)
		border.drawBox((0, 0, 0))
		border.render()

		text = Canvas.crop(area, x = 5, y = 5)
		self.text.render(text)