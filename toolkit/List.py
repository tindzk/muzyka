from Toolkit import *

def onSelect(list, idx):
	text = list.get(idx)

	print "Selected", idx
	print "Value:", text.getText()

surface = Surface(800, 400)
window  = GtkWindow(surface)

list = List()
list.setOnSelect(onSelect)
for i in range(1, 100):
	list.add(Text("Item " + str(i)))

top = VertPane()
top.addColumn(0.8, Text("List demo", weight = Text.Weight.Bold, orientation = Canvas.Orientation.Center))
top.addColumn(0.2, Text("13:37", weight = Text.Weight.Italic, orientation = Canvas.Orientation.Right))

pane = HorizPane()
pane.addRow(0.1, top)
pane.addRow(0.9, list)

window.addWidget(pane, 0, 0)
window.setActive(list)
window.render()
