This short introduction will help you to get muzyka started.
############################################################


Requirements:
	## Working implementation of Python 2.5++ (3.x not supported)
	## Working implementation of SQLAlchemy (See Part 1)
	## Working implementation of Flask-SQLAlchemy (See Part 2)
	## A copy of muzyka (See Part 3)

This setup how-to is based on the easy_install python script, available with the
'python-setuptools' package. Make sure you have it working before proceeding.

Part 1: Installation of SQLAlchemy
	execute 'easy_install SQLAlchemy' at a command prompt
	Test installation with a python shell:
		>>> import sqlalchemy
		>>> sqlalchemy.__version__
		'0.7.5'

Part 2: Installation of Flask-SQLAlchemy
	execute 'easy_install Flask-SQLAlchemy' at a command prompt
	Test installation with a python shell:
		>>> from flask import Flask
		>>> from flaskext.sqlalchemy import SQLAlchemy

Part 3: Get muzyka via git
	Install git
	$ git clone https://bitbucket.org/tindzk/muzyka.git

Fire up muzyka:
	Installation of muzykaWeb database:
		Go to $PROJECTROOT$/web/
		python createDatabase.py

	Run muzykaWeb Server
		Go to $PROJECTROOT$/web/
		python runserver.py

Access via http://localhost:5000
Profit!
