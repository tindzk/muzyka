from flaskext.sqlalchemy import SQLAlchemy
from flask import Flask, request, session, g, redirect, url_for, render_template, flash
from muzykaWeb import app, Playlist, db, PlaylistEntry

@app.route('/')
def index():
	return render_template('index.html')

# ===========================
# = Playlist relevant views =
# ===========================
	
@app.route('/playlists')
def playlists():
	playlists = Playlist.query.all()
	return render_template('playlists.html' , playlists = playlists)
	
# Creates a new playlist
# If you access this URL via GET, you will be prompted with a form to create a new playlist
# If you send POST data to this URL the incoming data will be interpreted
# as a request to create a new Playlist.

@app.route('/playlists/new', methods=['GET', 'POST'])
def new_playlist():
	error = None
	if request.method == 'POST':
		# app.debug(request.form['title'])
		if len(request.form['title']) > 0:
			db.session.add( Playlist( request.form['title'] ) )
			db.session.commit()
			
			flash('Playlist created!')
			return redirect(url_for('playlists'))
		else:
			error="Title is empty"
	
	playlists = Playlist.query.all()
	return render_template('new_playlist.html' , playlists = playlists, error=error)

# Shows a single playlist with all playlist entries

@app.route('/playlists/<id>')
def show_playlist(id):
	playlists = Playlist.query.all()
	playlist = Playlist.query.get(id)

	return render_template('show_playlist.html' , playlists = playlists, playlist = playlist)



# =================================
# = Playlist entry relevant views =
# =================================

# Creates a new playlist entry for a given Playlist
# If you access this URL via GET, you will be prompted with a form to create a new playlist entry.
# If you send POST data to this URL the incoming data will be interpreted
# as a request to create a new Playlist Entry.

@app.route('/playlists/<id>/playlistEntries/new', methods=['GET', 'POST'])
def new_playlist_entry(id):
	error = None
	playlist = Playlist.query.get(id)
	
	if request.method == 'POST':
		# app.debug(request.form['title'])
		if len(request.form['title']) > 0 and len(request.form['path']) > 0:
			db.session.add( PlaylistEntry( request.form['title'] , request.form['path'] , playlist ) )
			db.session.commit()
			
			flash('PlaylistEntry created!')
			return redirect(url_for('show_playlist' , id = playlist.id))
		else:
			error="Title or Path is empty"
	
	playlists = Playlist.query.all()
	return render_template('new_playlist_entry.html' , playlists = playlists, error=error)


# =========================
# = Player relevant views =
# =========================

@app.route('/player')
def player():
	return render_template('player.html')
	
# Play a given playlist entry

@app.route('/player/playPlaylistEntry/<id>')
def playPlaylistEntry_player(id):
	playlistEntryToPlay = PlaylistEntry.query.get(id)
	return render_template('player.html', playlistEntryToPlay = playlistEntryToPlay)
	

# @app.route