from flask import Flask, request, session, g, redirect, url_for, render_template, flash
from flaskext.sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
db = SQLAlchemy(app)

# Declare the Playlist Models in muzykaWeb
class Playlist(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), unique=True)

    def __init__(self, title):
        self.title = title

    def __repr__(self):
        return '<Playlist %r>' % self.title

# Declare the Playlist Entry Models in muzykaWeb,
# which are related to Playlists.
class PlaylistEntry(db.Model):
		id = db.Column(db.Integer, primary_key=True)
		title = db.Column(db.String(80))
		path = db.Column(db.String(250))

		playlist_id = db.Column(db.Integer, db.ForeignKey('playlist.id'))
		playlist = db.relationship('Playlist',backref=db.backref('playlistEntries', lazy='dynamic'))
		
		def __init__(self, title,path,playlist):
			self.title = title
			self.path = path
			self.playlist = playlist

		def __repr__(self):
			return '<PlaylistEntry %r>' % self.title

# Import ALL THE VIEWS! \o
import muzykaWeb.views
app.secret_key = 'TH!S!SSPARTA'