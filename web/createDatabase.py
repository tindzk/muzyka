# This file creates the neccessary sqlite database
# for the muzyka web interface.
# The path of the database is configured in muzykaWeb/__init__.py

from muzykaWeb import db
db.create_all()